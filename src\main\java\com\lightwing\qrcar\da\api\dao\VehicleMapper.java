package com.lightwing.qrcar.da.api.dao;

import com.lightwing.qrcar.da.api.model.Vehicle;

/**
 * This interface is to be used be myBatis as the namespace to which queries are implemented.
 */
public interface VehicleMapper {

    /**
     * Returns a Vehicle object from the database using the code passed in.
     *
     * @param code the code to query the database for a vehicle
     * @return a Vehicle object, or null if no record found
     */
    Vehicle getByCode(String code);
}
