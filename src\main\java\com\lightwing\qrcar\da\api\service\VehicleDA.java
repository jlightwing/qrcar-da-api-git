package com.lightwing.qrcar.da.api.service;

import com.lightwing.qrcar.da.api.model.Vehicle;

/**
 * An internal service to provide access to various methods that return models representing the data stored in
 * the VEHICLE table.
 */
public interface VehicleDA {

    /**
     * Retrieves a single Vehicle model from the database, where the business key (code) matches the one passed in
     * as a parameter.
     *
     * @param code the code to query the database with
     * @return a vehicle object representing the data, or null if nothing found
     */
    Vehicle getByCode(String code);
}
