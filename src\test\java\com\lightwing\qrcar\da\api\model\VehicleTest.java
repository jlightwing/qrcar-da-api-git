package com.lightwing.qrcar.da.api.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class VehicleTest {

    public static final String CODE = "Code";
    public static final BigDecimal ONE = BigDecimal.ONE;
    public static final String DESC = "Desc";
    public static final String MODEL = "Model";
    public static final String MAKE = "Make";

    @Test
    public void testPojo()
    {
        Vehicle pojo = new Vehicle();

        pojo.setCode(CODE);
        pojo.setPrice(ONE);
        pojo.setDesc(DESC);
        pojo.setModel(MODEL);
        pojo.setMake(MAKE);

        assertEquals(CODE, pojo.getCode());
        assertEquals(ONE, pojo.getPrice());
        assertEquals(DESC, pojo.getDesc());
        assertEquals(MODEL, pojo.getModel());
        assertEquals(MAKE, pojo.getMake());
    }
}
